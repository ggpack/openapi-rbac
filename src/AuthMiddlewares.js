const cookieJs = require("cookie")
const { encodeObj } = require("./SecurityValidator")


// Simple volatile storage
const sessions = {}


function addSession(sessionId, user)
{
	sessions[sessionId] = user
}

const random = () => (Math.random() + 1).toString(36).substring(7);

// From a matching sessionId in the cookie or undefined
function getTokens(cookie)
{
	const cookies = cookieJs.parse(cookie || "")
	const sessionId = cookies.session
	return [sessions[sessionId], sessionId]
}

module.exports = app =>
{

	// Frontal mdw getting all requests, kind of like a proxy
	app.use((req, res, next) =>
	{
		// Prevent any auth injection
		delete req.headers.authorization

		const [user] = getTokens(req.headers.cookie)

		// Logged-in or anonymous have a authorization header past this point
		req.headers.authorization = "bearer " + encodeObj(user)

		next()
	})

	// Oversimplify an oauth flow for the demo
	app.get("/.auth/login",
		async (req, res) =>
		{
			const roles = req?.query?.roles
			console.log("Success, enroll the user as a", roles)
			const sessionId = random()

			addSession(sessionId,
				{
					id: random(),
					roles: roles?.split(",") || []
				}
			)

			console.log("\x1b[32;1mCreate a user for sessionId\x1b[0m", sessionId)

			res.cookie("session", sessionId, {maxAge: 1000 * 60 * 60 * 24 * 7})

			res.statusCode = 200
			res.end()
		}
	)

	app.use("/.auth/logout",
		(req, res) =>
		{
			const [user, sessionId] = getTokens(req.headers.cookie)

			console.log("logout", user, sessionId)
			// Logging & cleanup
			let name = "anonymous" || user?.id
			delete sessions[sessionId]
			console.log("Logout", name)

			res.clearCookie("session")
			res.end()
		}
	)
}
