const paramMapping =
{
	path: req => req.params,
	header: req => req.headers,
}


class Controller
{
	static response(code, payload, headers)
	{
		return { code, payload, headers }
	}

	static sendResponse(response, svcRes)
	{
		response.status(svcRes.code)
		response.set(svcRes.headers)
		response.json(svcRes.payload)
	}

	static collectRequestParams(request)
	{
		const requestParams = {}
		if(request.openapi.schema.requestBody)
		{
			requestParams.body = request.body
		}

		for(const param of request.openapi.schema.parameters || [])
		{
			const getParam = paramMapping[param.in] || (req => req[param.in])
			requestParams[param.name] = getParam(request)?.[param.name]
		}
		return requestParams
	}

	static async handleRequest(request, response, serviceOperation)
	{
		try
		{
			const serviceResponse = await serviceOperation(request, Controller.collectRequestParams(request))
			Controller.sendResponse(response, serviceResponse)
		}
		catch(excep)
		{
			console.error("Internal exception", excep)
			const code = 500
			const payload = "Internal error"
			Controller.sendResponse(response, {code, payload})
		}
	}

	// Shorthand for services
	static makeHandler(serviceOperation)
	{
		return (req, res) => Controller.handleRequest(req, res, serviceOperation)
	}
}

module.exports = Controller
