const Controller = require("./Controller")

const resources = [0, 1, 2]

function getResourcesSvc(req)
{
	console.log("Getting the resources")
	return Controller.response(200, resources)
}

function createResourceSvc(req)
{
	console.log("Adding a resource")
	newRsc = `${req?.user?.id ?? "anonymous"} was here`
	resources.push(newRsc)
	return Controller.response(201, newRsc)
}

module.exports = {
	getResources: Controller.makeHandler(getResourcesSvc),
	createResource: Controller.makeHandler(createResourceSvc),
}
