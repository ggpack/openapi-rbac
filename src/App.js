const express = require("express")
const validator = require("express-openapi-validator")
const fs = require("fs")
const yaml = require("js-yaml")
const path = require("path")
const swaggerUI = require("swagger-ui-express")

const authMiddlewaresRegisterer = require("./AuthMiddlewares")
const { securityHandlers } = require("./SecurityValidator")

function logReq(req, res, next)
{
	console.log("New request:", req.method, req.path, req.headers.cookie)
	next()
}


// Uncaught exceptions like openapi validation errors
function globalErrorHandler(err, req, res, next)
{
	console.log("Global error", err)
	res.status(err.status || 500).json({
		message: err.message || err,
		errors: err.errors || "",
	})
}


function newApp()
{
	const app = express()
	const apiSpec = path.join(__dirname, "api", "openapi.yml")
	const apiObj = yaml.safeLoad(fs.readFileSync(apiSpec))

	app.disable("x-powered-by")
	app.use(logReq)

	authMiddlewaresRegisterer(app)

	app.use(express.json({limit: '25mb'}))
	app.use(express.urlencoded({ extended: false }))

	app.use("/api-docs/types", express.static(__dirname + "/api/types"))
	app.use("/api-docs", swaggerUI.serve, swaggerUI.setup(apiObj))

	const apiEndpoints = validator.middleware({
		apiSpec,
		operationHandlers: __dirname,
		validateSecurity: { handlers: securityHandlers },
	})

	app.use(apiEndpoints)
	app.use(globalErrorHandler)
	return app
}

module.exports = newApp()
