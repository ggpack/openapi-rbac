const app = require("./App")

async function main()
{
	const port = 8080
	const server = app.listen(port)
	console.info(`Express server running on port ${port}`)

	process.on("SIGINT", () =>
	{
		console.log("Caught interrupt signal")
		server.close()
	})
}

main()
