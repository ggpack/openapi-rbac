const crypto = require("node:crypto")
// Doc https://github.com/cdimascio/express-openapi-validator/wiki/Documentation#%EF%B8%8F-validatesecurity-optional

// Returns true if l1 includes at least one elem of l2
const hasSome = (l1, l2) => l2?.some(e2 => l1?.includes(e2))

const encodeObj = obj => Buffer.from(JSON.stringify(obj || {})).toString("base64")

const decodeObj = strB64 => JSON.parse(Buffer.from(strB64, "base64").toString("utf-8"))

const getClaims = jwtStr => decodeObj(jwtStr?.split(".")[1])

// Anonymous user logic is business-specific.
// Here, we let an anonymous user pass but check further if any role is required.
// The token is in our custom format
function userAuth(req, scopes/*, schema*/)
{
	// Empty scope list means anonymous is OK
	const noCheck = !scopes?.length

	// Ill-formed auth
	if(!req.headers.authorization){ return noCheck }

	const userToken = req.headers.authorization.split(" ")[1]
	// Ill-formed auth
	if(!userToken){ return noCheck }

	const user = decodeObj(userToken)

	// Ill-formed user
	if(!user?.id || !user?.roles?.length){ return noCheck }

	req.user = user // For logging & RBAC
	const rbacResult = noCheck || hasSome(user.roles, scopes)
	console.log(`\x1b[32;1mSECU\x1b[0m: Access ${rbacResult ? "granted" : "denied"} to '${user.id}'`)
	return rbacResult
}


module.exports =
{
	securityHandlers:
	{
		userAuth,
	},
	decodeObj,
	encodeObj,
	getClaims,
}
