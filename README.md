Demonstrates a minimal declarative RBAC in the openapi.yml file.

The JWT validation is done by the Security validator.

The login flow is a truncated OAuth code flow, that always give a user the requested roles, for testing purpose.

# Test

Go check the test folder
